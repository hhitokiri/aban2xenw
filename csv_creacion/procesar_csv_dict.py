# -*- coding: utf-8 -*-
import os
import csv
from settings import *

class ProcesarCsv(object):
	"""docstring for ProcesarCsv"""
	def __init__(self):
		self.datos_csv_aban = ABANQ #archivo productos de abanq
		self.datos_prodct_product = PRODUCT_PRODUCT#archivo base de openerp


	def datos_csv(self, nombre, modo):
		
		datos_csv=open(nombre, modo)
		datos = csv.DictReader(datos_csv, delimiter=',', quotechar='"', escapechar='\\')

		return datos

	def escribir_datos_product(self):
		count = 0
		aban = self.datos_csv(self.datos_csv_aban, 'r')
		product = self.datos_csv(self.datos_prodct_product, 'r')
		# for x in aban:
		# 	print x
		nombre_orden = product.fieldnames
		with open(CARPETA_FILE + 'productos1.csv', 'wb') as nuevo:
		 	for plantilla in product:
		 		#nuevo_product = csv.DictWriter(nuevo,quotechar='"', quoting=csv.QUOTE_ALL, fieldnames = nombre_orden)
		 		nuevo_product = csv.DictWriter(nuevo,  fieldnames = nombre_orden)
		 		nuevo_product.writerow(dict(( _, _ ) for _ in nombre_orden))
		 		valores_plantilla = plantilla
		 		for valor_master in  aban:
		 			if valor_master['stockfis'] == 'NULL':
		 				valor_master.update({'stockfis': '0.0000000'})
		 			if valor_master['pvp'] == 'NULL':
		 				valor_master.update({'pvp': '0.0000000'})
		 			valores_plantilla.update({'id':'%s' % count})
		 			valores_plantilla.update({'modelo':'%s' % valor_master['referencia']})
		 			valores_plantilla.update({'otras_caracteristicas':'%s' % valor_master['descripcion']})
		 			valores_plantilla.update({'precio':'%.2f' % float(valor_master['pvp'])})
		 			valores_plantilla.update({'cantidad':'%.2f' % float(valor_master['stockfis'])})
		 			nuevo_product.writerow(valores_plantilla)
		 			count += 1
		 		break
	


